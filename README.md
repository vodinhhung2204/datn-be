# DATN- BE
> DATN-BE
## Database
* Use Postgresql, with Knexjs as query builder
* pgAdmin4 must be installed to better manage Postgresql relational databases
* Connection string will be stored in knexfile.js
* Remember to update env variables

## Migrations
To create new migration, run yarn script:

```
$ knex migrate:make <migration-name>
```

To run the migrations, run yarn script:

```
$ knex migrate:latest
```

To undo the migrations, run yarn script:

```
$ knex migrate:rollback
```

## Seeds
To create new seed file, run yarn script:

```
$ knex seed:make <seed-name>
```

To run the seed, run yarn script:

```
$ knex seed:run
```