import jwt from 'jsonwebtoken';
import { SECRET_KEY, TOKEN_DURATION } from '@app/utils/env.constants.utils';

export const generateToken = (payload) => {
    return jwt.sign({ id: payload }, SECRET_KEY, { expiresIn: TOKEN_DURATION });
};

export const verifyToken = (payload) => {
    return jwt.decode(payload, SECRET_KEY);
};

export const validateToken = (accessToken) => {
    if (!accessToken) {
        throw new Error('Your token is not supplied');
    }
    let token = accessToken;
    if (token.startsWith('Bearer ')) {
        token = token.replace('Bearer ', '').trim();
    }
    const payload = verifyToken(token);
    if (!payload) {
        throw new Error('Your token is not valid');
    }
    return payload;
};
