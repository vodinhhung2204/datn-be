import express from 'express';
import knex from '@app/database/connection';

const router = express.Router();

router.get('/', (req,res)=>{
    res.render('index.ejs');
})
router.get('/listCategory', async (req,res)=>{
    var dataCategory = [];
    dataCategory = await knex.select().from('categorys')
    res.render('pages/Categorys/listCategory.ejs',{data:dataCategory});
})
router.get('/listPosts', async (req,res)=>{
    var dataPost = [];
    dataPost = await knex.select().from('posts')
    res.render('pages/Posts/listPosts.ejs',{data:dataPost});
})
export default router