/* eslint-disable no-console */
/**
 * Module dependencies.
 */
import 'module-alias/register';
import app from '@app/index';
import debugLib from 'debug';
import http from 'http';
import { EXIT_CODE } from '@app/utils/constants.utils';
import { parseDecimalNumber } from '@app/utils/helpers.utils';

const debug = debugLib('datn:server');

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
    const port = parseDecimalNumber(val);
    if (Number.isNaN(port)) {
        // named pipe
        return val;
    }

    if (port >= 0) {
        // port number
        return port;
    }

    return false;
}

/**
 * Get port from environment and store in Express.
 */

const port = normalizePort(process.env.PORT || '3000');
app.set('port', port);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    const bind = typeof port === 'string'
        ? `Pipe ${port}`
        : `Port ${port}`;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(`${bind} requires elevated privileges`);
            process.exit(EXIT_CODE.FAILURE);
            break;
        case 'EADDRINUSE':
            console.error(`${bind} is already in use`);
            process.exit(EXIT_CODE.FAILURE);
            break;
        default:
            throw error;
    }
}

/**
 * Create HTTP server.
 */

const server = http.createServer(app);

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string'
        ? `pipe ${addr}`
        : `port ${addr.port}`;
    debug(`Listening on ${bind}`);
}

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port, () => {
    console.log(`Server is listening on port: ${port}`);
});
server.on('error', onError);
server.on('listening', onListening);
