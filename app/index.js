import express from 'express';
import initConfig from '@app/config/init.config';
import cors from 'cors';

import 'dotenv/config';

const app = express();
app.use(cors());

initConfig(app);

export default app;
