import httpStatus from 'http-status';
import { DEFAULT_VALUE, ERROR_MESSAGE, REGEX } from '@app/utils/constants.utils';
import * as jwt from '@app/services/jwt';
import { multerUploader } from '@app/config/multer.config';
import { MulterError } from 'multer';
import { parseDecimalNumber, handleErrorResponse } from '@app/utils/helpers.utils';

function isValidNumber(number) {
    if (!number) {
        return true;
    }
    const pattern = REGEX.NUMBER_ONLY;
    if (pattern.test(number)) {
        return true;
    }
    return false;
}

function assignValue(number, defaultValue) {
    if (!number) return defaultValue;
    if (parseDecimalNumber(number) === 0) return defaultValue;
    return number;
}

export function isValidPageLimit(req, res, next) {
    let { page, limit } = req.query;
    if (!isValidNumber(page)) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.INVALID_PAGE);
    }
    req.query.page = assignValue(page, DEFAULT_VALUE.PAGE) - 1;
    if (!isValidNumber(limit)) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.INVALID_LIMIT);
    }
    req.query.limit = assignValue(limit, DEFAULT_VALUE.LIMIT);
    next();
}

export function validateToken(req, res, next) {
    try {
        const accessToken = req.headers['authorization'] || req.headers['x-access-token'];
        const tokenPayload = jwt.validateToken(accessToken);
        req.payload = tokenPayload;
        return next();
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}

export function validateMulterError(req, res, next) {
    multerUploader(req, res, (err) => {
        if (err instanceof MulterError || err instanceof Error) {
            if (err.code === 'LIMIT_UNEXPECTED_FILE') {
                return handleErrorResponse(res,
                    httpStatus.BAD_REQUEST,
                    ERROR_MESSAGE.MAXIMUM_IMAGE);
            }
            return handleErrorResponse(res, httpStatus.BAD_REQUEST, err.message);
        }
        const { files } = req;
        if (!files.length) {
            return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.MISSING_IMAGE);
        }
        return next();
    });
}
