import express from 'express';
import * as CategoriesController from '@app/src/api/categories/categories.controller';

const router = express.Router();

router.get('/',CategoriesController.getCategories)
export default router;
