import * as CategoriesRepository from '@app/src/api/categories/categories.repositories';
import { ERROR_MESSAGE } from '@app/utils/constants.utils';

export function getCategories() {
    try {
        return CategoriesRepository.getCategories();
    } catch (error) {
        throw new Error(ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
