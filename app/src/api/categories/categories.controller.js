import httpStatus from 'http-status';
import * as categoriesService from '@app/src/api/categories/categories.services';
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from '@app/utils/constants.utils';
import { handleErrorResponse } from '@app/utils/helpers.utils';

export async function getCategories(req, res) {
    try {
        let categories = await categoriesService.getCategories();
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.FETCHED,
            data: categories
        });
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
