import knex from '@app/database/connection';

export function getCategories() {
    return knex('categorys')
        .select('categorys.id as id',
            'categorys.name as name',
            'categorys.link as link')
}
