import {
 ERROR_MESSAGE, LENGTH, DEFINED_VALUE, ACTION
} from '@app/utils/constants.utils';
import { body, validationResult } from 'express-validator';
import httpStatus from 'http-status';
import * as userRepository from '@app/src/api/users/users.repositories';
import { parseDecimalNumber, handleErrorResponse } from '@app/utils/helpers.utils';

export function validateRegisterInput() {
    return [
        body('username', `Invalid username, minimum length is ${LENGTH.USERNAME}`).exists().isLength({ min: LENGTH.USERNAME }),
        body('password', `Invalid password, minimum length is ${LENGTH.PASSWORD}`).exists().isLength({ min: LENGTH.PASSWORD }),
        body('email', 'Invalid email').exists().isEmail(),
        body('phone', 'Invalid phone number').isDecimal().exists().isLength({ max: LENGTH.PHONE }),
        //body('displayName', 'Invalid display name').exists().isLength({ min: LENGTH.DISPLAY_NAME })
    ];
}

export function validateLoginInput() {
    return [
        body('username', `Invalid username, minimum length is ${LENGTH.USERNAME}`).exists().isLength({ min: LENGTH.USERNAME }),
        body('password', `Invalid password, minimum length is ${LENGTH.PASSWORD}`).exists().isLength({ min: LENGTH.PASSWORD })
    ];
}

export function validateResult(req, res, next) {
    const validationErrors = validationResult(req);
    if (!validationErrors.isEmpty()) {
        const validationMessage = validationErrors.array();
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, validationMessage);
    }
    next();
}

export async function validateExistedUsername(req, res, next) {
    try {
        const { username } = req.body;
        const usernameExisted = await userRepository.usernameExisted(username);
        const userCount = parseDecimalNumber(usernameExisted[0].count);
        if (userCount === DEFINED_VALUE.EXISTED) {
            return handleErrorResponse(res,
                httpStatus.BAD_REQUEST,
                ERROR_MESSAGE.DUPLICATE_USERNAME);
        }
        next();
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}

export async function isValidUser(req, res, next) {
    try {
        const userId = req.payload.id;
        if (!userId) {
            return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.MISSING_ID);
        }
        const validIdUser = await userRepository.isValidId(userId);
        const count = parseDecimalNumber(validIdUser.count);
        if (!count) {
            return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.INVALID_ID);
        }
        next();
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
