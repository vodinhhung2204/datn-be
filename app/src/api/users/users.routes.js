import express from 'express';
import * as userController from '@app/src/api/users/users.controllers';
import * as userMiddleware from '@app/src/api/users/users.middlewares';
const router = express.Router();

router.post('/register',
            userMiddleware.validateRegisterInput(),
            userMiddleware.validateResult,
            userMiddleware.validateExistedUsername,
            userController.register);
router.post('/login', userMiddleware.validateLoginInput(), userMiddleware.validateResult, userController.login);

export default router;
