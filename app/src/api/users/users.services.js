import * as userRepository from '@app/src/api/users/users.repositories';
import bcrypt from 'bcrypt';
import { ERROR_MESSAGE, DEFINED_VALUE } from '@app/utils/constants.utils';
import * as Jwt from '@app/services/jwt';

export async function register(inputFields) {
    try {
        const { password, ...otherFields } = inputFields;
        const hashPassword = bcrypt.hashSync(password, DEFINED_VALUE.ROUNDS);
        const inputFieldsWithHash = { hashPassword, ...otherFields };
        console.log(inputFieldsWithHash)
        const createAccount = await userRepository.register(
            inputFieldsWithHash
        );
        return createAccount.rowCount;
    } catch (error) {
        throw new Error(ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}


export async function login({ username, password }) {
    try {
        const user = await userRepository.getProfile(username);

        if (!user) {
            throw new Error(ERROR_MESSAGE.INVALID_CREDENTIAL);
        }
        const dbpassword = user.password;
        const isMatch = await bcrypt.compareSync(password, dbpassword);
        console.log(isMatch)
        if (!isMatch) {
            throw new Error(ERROR_MESSAGE.INVALID_CREDENTIAL);
        }
        const {
            id, email, phone, displayName
        } = user;

        const token = Jwt.generateToken(id);
        return {
            id,
            email,
            phone,
            displayName,
            token
        };
    } catch (error) {
        throw new Error(error.message);
    }
}
