import knex from '@app/database/connection';
import { ROLE } from '@app/utils/constants.utils';

export function register(inputFields) {
    const {
        username,
        hashPassword,
        email,
        phone,
        displayName,
    } = inputFields;
    return knex('users').insert({
        username: username,
        password: hashPassword,
        email: email,
        phone: phone,
        displayName: displayName,
        idRole: 2,
    });
}

export function usernameExisted(username) {
    return knex('users')
            .count('id')
            .where('username', username);
}

export async function getProfile(username) {
    try {
        return await knex.select('id',
            'password',
            'email',
            'phone',
            'displayName').from('users').where({ username: username }).first();
    } catch (error) {
        throw new Error(error.detail);
    }
}

export function isValidId(id) {
    return knex('users')
        .count('id')
        .where('id', id)
        .first();
}

export async function getIdRole(idUser) {
    try {
        return await knex('users').where({ id: idUser }).select('idRole');
    } catch (error) {
        throw new Error(error.detail);
    }
}

export async function getAction(idRole) {
    try {
        return await knex('actions')
                     .select('actions.name')
                     .innerJoin('roles_actions', 'actions.id', 'roles_actions.idAction')
                     .where('roles_actions.idRole', idRole);
    } catch (error) {
        throw new Error(error.detail);
    }
}
