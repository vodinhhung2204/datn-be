import httpStatus from 'http-status';
import * as userService from '@app/src/api/users/users.services';
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from '@app/utils/constants.utils';
import * as helperFunctions from '@app/utils/helpers.utils';
import * as whitelist from '@app/config/whitelist.config';
import { handleErrorResponse,filterBody } from '@app/utils/helpers.utils';

export async function register(req, res) {
    try {
        const inputFields = helperFunctions.filterBody(req.body, whitelist.USER_FIELDS);
        await userService.register(inputFields);
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.CREATED
        });
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}


export async function login(req, res) {
    try {
        const inputField = filterBody(req.body, whitelist.LOGIN_FIELDS);
        const result = await userService.login(inputField);
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.LOGGED_IN,
            data: result
        });
    } catch (error) {
        if (error.message === ERROR_MESSAGE.INVALID_CREDENTIAL) {
            return handleErrorResponse(res,
                httpStatus.UNAUTHORIZED,
                ERROR_MESSAGE.INVALID_CREDENTIAL);
        }
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
