import httpStatus from 'http-status';
import * as PostsService from '@app/src/api/posts/posts.services';
import { ERROR_MESSAGE, SUCCESS_MESSAGE } from '@app/utils/constants.utils';
import { handleErrorResponse } from '@app/utils/helpers.utils';

export async function getPostsByCategory(req, res) {
    try {
        const { orderBy,value }=req.query
        let Posts = [];
        switch (orderBy) {
            case 'category':
                Posts = await PostsService.getPostsByCategory(
                    value
                );
                break;
            default:
                return handleErrorResponse(res,
                    httpStatus.BAD_REQUEST,
                    ERROR_MESSAGE.CLIENT_MESSAGE);
        }
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.FETCHED,
            data: Posts
        });
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
export async function getPostDetails(req, res) {
    try {
        const { id } = req.params;
        let postDetails = await PostsService.getPostDetails(id);
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.FETCHED,
            data: postDetails
        });
    } catch (error) {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}