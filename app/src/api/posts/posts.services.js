import * as PostsRepository from '@app/src/api/posts/posts.repositories';
import { ERROR_MESSAGE } from '@app/utils/constants.utils';

export function getPostsByCategory(value) {
    try {
        return PostsRepository.getPostsByCategory(value);
    } catch (error) {
        throw new Error(ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}
export function getPostDetails(id) {
    try {
        return PostsRepository.getPostDetails(id);
    } catch (error) {
        throw new Error(ERROR_MESSAGE.CLIENT_MESSAGE);
    }
}