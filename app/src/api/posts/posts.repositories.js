import knex from '@app/database/connection';

export function getPostsByCategory(value) {
    return knex('posts')
        .select('posts.id as id',
            'posts.idcategory as idcategory',
            'posts.title as title',
            'posts.content as content',
            'image',
            'posts.createdAt')
        .innerJoin('categorys','categorys.id', 'posts.idcategory')
        .where('posts.idcategory',value)
 }
 export function getPostDetails(id) {
    return knex('posts')
        .select('posts.id as id',
        'posts.idCategory as idCategory',
        'posts.title as title',
        'posts.Content as Content',
        'image',
        'posts.createdAt')
        .innerJoin('categorys','categorys.id', 'posts.idCategory')
        .where('posts.id',id)
        .first();
}
