import express from 'express';
import * as PostsController from '@app/src/api/posts/posts.controller';

const router = express.Router();

router.get('/',PostsController.getPostsByCategory)
router.get('/:id',PostsController.getPostDetails);
export default router;
