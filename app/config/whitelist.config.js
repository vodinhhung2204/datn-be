export const USER_FIELDS = [
    'username',
    'password',
    'email',
    'phone',
    'displayName',
    'idRole',
    'Content'
];
export const LOGIN_FIELDS = [
    'username',
    'password'
];