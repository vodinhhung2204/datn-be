import express from 'express';
import userRouter from '@app/src/api/users/users.routes';
import categoryRouter from '@app/src/api/categories/categories.routes';
import postRouter from '@app/src/api/posts/posts.routes';

const router = express.Router();

router.use('/users', userRouter);
router.use('/categories', categoryRouter);
router.use('/posts', postRouter);



export default router;
