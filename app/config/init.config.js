import bodyParser from 'body-parser';
import routeConfig from '@app/config/routes.config';
import httpStatus from 'http-status';
import helmet from 'helmet';
import { SUCCESS_MESSAGE, ERROR_MESSAGE } from '@app/utils/constants.utils';
import { jwt } from '@app/utils/jwt.utils';
import { errorHandler } from '@app/utils/error.handlers.utils';
import routeAdmin from '@app/config/routes.admin';
import express from 'express';
export default (app) => {
    app.use(helmet());
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use(jwt());
    app.set('view engine', 'ejs');
    app.use(errorHandler);
    app.use('/plugins', express.static('app/admin/public/plugins'))
    app.use('/dist', express.static('app/admin/public/dist'))
    app.set('views','app/admin/views');
    app.use('/api', routeConfig);
    app.use('/admin',routeAdmin);
    app.get('/', (req, res) => {
        return res.status(httpStatus.OK).json({
            message: SUCCESS_MESSAGE.SERVER_STARTED
        });
    });
    app.use('*', (req, res) => {
        return res.status(404).json({
            message: ERROR_MESSAGE.ENDPOINT_NOT_FOUND
        });
    });
};
