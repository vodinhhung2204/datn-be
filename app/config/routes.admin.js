import express from 'express';
import admin from '@app/admin/routes/index'

const router = express.Router();

router.use('/', admin);

export default router;
