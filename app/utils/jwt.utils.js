import expressJwt from 'express-jwt';
import { SECRET_KEY } from '@app/utils/env.constants.utils';
import { REGEX } from '@app/utils/constants.utils';

export function jwt() {
    return expressJwt({ secret: SECRET_KEY, algorithms: ['HS256'] }).unless({
        path: [
            {
                url: REGEX.ROOT_ENDPOINT,
                methods: ['GET']
            },
            {
                url: REGEX.USER_ENDPOINT,
                methods: ['GET', 'POST']
            },
            {
                url: REGEX.POST_ENDPOINT,
                methods: ['GET']
            }
        ]
    });
}
