export const DEFINED_VALUE = {
    EXISTED: 1,
    RADIX: 10,
    ROUNDS: 10,
    IMAGES: 'images',
    MAX_UPLOAD: 5,
    MAX_FILE_SIZE: 5000000
};
export const LENGTH = {
    PHONE: 20,
    PASSWORD: 8,
    USERNAME: 8,
    DISPLAY_NAME: 8,
    CAR_POST_NAME: 8,
    CAR_POST_YEAR: 4,
    CAR_POST_FUEL_TYPE: 2,
    CAR_POST_DISTANCE: 3,
    CAR_POST_PRICE: 3,
    CAR_POST_LOCATION: 5
};
export const ERROR_MESSAGE = {
    ROUTE: 'Error at route handler',
    CONTROLLER: 'Error at controller',
    MIDDLEWARE: 'Error at middleware',
    SERVICE: 'Error at service',
    REPOSITORY: 'Error at repository',
    CLIENT_MESSAGE: 'Error while handling request',
    ENDPOINT_NOT_FOUND: 'Not found',
    INVALID_ID: 'Invalid Id',
    MISSING_ID: 'Id is missing',
    UNAUTHORIZED: 'Please login to continue',
    INVALID_CREDENTIAL: 'Wrong username or password',
    INVALID_PAGE: 'Invalid input for page',
    INVALID_LIMIT: 'Invalid input for limit',
    DUPLICATE_USERNAME: 'This username has been taken',
    INVALID_TOKEN: 'Token is invalid',
    UNSUPPORTED_FILE_TYPE: 'Unsupported file extension',
    MISSING_IMAGE: 'Image cannot be null',
    MAXIMUM_IMAGE: `Number of upload images exceed maximum of ${DEFINED_VALUE.MAX_UPLOAD} images`,
    INVALID_AUTHOR: 'You can not update this post because you do not own this!',
    INVALID_PRIVILEGE: 'You dont have permission'
};

export const SUCCESS_MESSAGE = {
    CREATED: 'Successfully created',
    DELETED: 'Successfully deleted',
    SERVER_STARTED: 'Hello, this is the my DATN-BE',
    LOGGED_IN: 'Successfully logged in',
    FETCHED: 'Successfully fetched',
    UPDATE_POST: 'Successfully updated post'
};
export const REGEX = {
    NUMBER_ONLY: /^\d+$/,
    ROOT_ENDPOINT: /^/,
    USER_ENDPOINT: /^\/api\/users.*/,
    POST_ENDPOINT: /^\/api\/posts.*/
};
export const DIR_PATH = {
    UPLOAD: './app/upload'
};
