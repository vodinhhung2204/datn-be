import fs from 'fs';
import { DEFAULT_VALUE, DEFINED_VALUE } from '@app/utils/constants.utils';
import escape from 'url-escape-tag';

export function filterBody(userInputs, fieldsToFilter) {
    const userFields = Object.keys(userInputs);
    let result = {};
    userFields.forEach(element => {
        if (fieldsToFilter.includes(element)) {
            result[element] = userInputs[element];
        }
    });
    return result;
}

export function filterFiles(request, fieldsToFilter) {
    const { files } = request;
    let result = [];
    files.forEach(element => {
        if (fieldsToFilter.includes(element.fieldname)) {
            result.push(element);
        }
    });
    return result;
}

export function removingBulkFiles(paths) {
    paths.forEach(path => {
        fs.unlinkSync(path);
    });
}

export function getFileExtension(mimetype) {
    if (!mimetype.split('/').length) {
        return DEFAULT_VALUE.EXTENSION;
    }
    const extension = mimetype.split('/')[1];
    return extension;
}

export function parseDecimalNumber(string) {
    return parseInt(string, DEFINED_VALUE.RADIX);
}

export function handleErrorResponse(response, error, message) {
    return response.status(error).json({
        message: message
    });
}

export function handleEscapeString(string) {
    return escape`${string}`;
}
