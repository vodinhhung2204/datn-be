/* eslint-disable no-unused-vars */
import httpStatus from 'http-status';
import { ERROR_MESSAGE } from '@app/utils/constants.utils';
import { handleErrorResponse } from '@app/utils/helpers.utils';

export function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        return handleErrorResponse(res, httpStatus.BAD_REQUEST, err);
    }

    if (err.name === 'UnauthorizedError') {
        return handleErrorResponse(res, httpStatus.UNAUTHORIZED, ERROR_MESSAGE.INVALID_TOKEN);
    }
    return handleErrorResponse(res, httpStatus.INTERNAL_SERVER_ERROR, err.message);
}
