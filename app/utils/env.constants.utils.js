import 'dotenv/config';

export const SECRET_KEY = process.env.ACCESS_TOKEN_SECRET;
export const TOKEN_DURATION = process.env.JWT_TOKEN_DURATION;
export const NODE_ENV = process.env.NODE_ENV;
