exports.up = (knex) => knex.schema.createTable('categorys', (table) => {
    table.increments('id').primary();
    table.string('name').notNullable();
    table.string('link').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
});

exports.down = (knex) => knex.schema.dropTableIfExists('categorys');
