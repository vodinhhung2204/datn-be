exports.up = (knex) => knex.schema.createTable('users', (table) => {
    table.increments('id').primary();
    table.string('username').notNullable();
    table.string('password').notNullable();
    table.string('email').notNullable();
    table.string('phone', 20).notNullable();
    table.string('displayName');
    table.integer('idrole').notNullable();
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    // indices
    table.foreign('idrole').references('roles.id').onDelete('CASCADE').onUpdate('CASCADE');
});

exports.down = (knex) => knex.schema.dropTableIfExists('users');
