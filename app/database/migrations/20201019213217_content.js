exports.up = (knex) => knex.schema.createTable('contents', (table) => {
    table.increments('id').primary();
    table.integer('iduser');
    table.text('content');
    // indices
    table.foreign('iduser').references('users.id').onDelete('CASCADE').onUpdate('CASCADE');
});

exports.down = (knex) => knex.schema.dropTableIfExists('contents');
