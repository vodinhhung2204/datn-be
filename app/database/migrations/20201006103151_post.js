exports.up = (knex) => knex.schema.createTable('posts', (table) => {
    table.increments('id').primary();
    table.integer('idcategory').notNullable();
    table.integer('iduser');
    table.string('title').notNullable();
    table.string('link').notNullable();
    table.json('content');
    table.string('image').notNullable();
    table.string('tag',2000)
    table.timestamp('createdAt').defaultTo(knex.fn.now());
    table.timestamp('updatedAt').defaultTo(knex.fn.now());
    table.timestamp('deletedAt');
    // indices
    table.foreign('idcategory').references('categorys.id').onDelete('CASCADE').onUpdate('CASCADE');
    table.foreign('iduser').references('users.id').onDelete('CASCADE').onUpdate('CASCADE');
});

exports.down = (knex) => knex.schema.dropTableIfExists('posts');
