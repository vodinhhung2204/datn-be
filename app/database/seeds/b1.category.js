
exports.seed = function(knex) {
  // Deletes ALL existing entries
  return knex('categorys').del()
    .then(function () {
      // Inserts seed entries
      return knex('categorys').insert([
        {
          id: 1,
          name: "Star",
          link: "/star.chn"
      },
      {
          id: 2,
          name: "TV Show",
          link: "/tv-show.chn"
      },
      {
          id: 3,
          name: "Ciné",
          link: "/cine.chn"
      },
      {
          id: 4,
          name: "Musik",
          link: "/musik.chn"
      },
      {
          id: 5,
          name: "Beauty & Fashion",
          link: "/beauty-fashion.chn"
      },
      {
          id: 6,
          name: "Xem Mua Luôn",
          link: "/xem-mua-luon.chn"
      },
      {
          id: 7,
          name: "Đời sống",
          link: "/doi-song.chn"
      },
      {
          id: 8,
          name: "Ăn - Quẩy - Đi",
          link: "/an-quay-di.chn"
      },
      {
          id: 9,
          name: "Xã hội",
          link: "/xa-hoi.chn"
      },
      {
          id: 10,
          name: "Thế giới",
          link: "/the-gioi.chn"
      },
      {
          id: 11,
          name: "Sport",
          link: "/sport.chn"
      },
      {
          id: 12,
          name: "Học đường",
          link: "/hoc-duong.chn"
      }
      ]);
    });
};
