exports.seed = (knex) => {
  // Deletes ALL existing entries
  return knex('users').del()
    .then(() => {
      // Inserts seed entries
      return knex('users').insert([
        {
          username: 'vodinhhung2204',
          password: 'vodinhhung',
          email: 'vodinhhung2204@gmail.com',
          phone: '0765333976',
          displayName: 'Võ Đình Hùng',
          idrole: 1
        },
        {
          username: 'demo',
          password: 'demo',
          email: 'demo@gmail.com',
          phone: '0765333976',
          idrole: 2
        },
        {
          username: 'demo2',
          password: 'demo2',
          email: 'demo2@gmail.com',
          phone: '0765333976',
          idrole: 2
        }
      ]);
    });
};
