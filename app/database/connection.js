import knexfile from '@root/knexfile';
import knex from 'knex';
import { NODE_ENV } from '@app/utils/env.constants.utils';

const environment = NODE_ENV || 'development';
const config = knexfile[environment];

export default knex(config);
